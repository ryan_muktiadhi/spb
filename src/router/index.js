import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import NavigatorLayout from '../layouts/NavigatorLayout.vue'

const routes = [
  { path: '', redirect: '/home' },
  {
    path: '/',
    component: NavigatorLayout,
    beforeEnter: (to, from, next) => {
      // ...
      next();
    },
    children:[
      {path:'home', component: HomeView },
      {path:'about', component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue') },
      {path:'contact',component: () => import(/* webpackChunkName: "contact" */ '../views/ContactView.vue') },
    ]
  },
  {
    path: '/auth',
    component: () => import('../layouts/NoNavigatorLayout.vue'),
    beforeEnter: (to, from, next) => {
      // ...
      next();
    },
    children:[
      {path:'login', component: () => import('../views/login/LoginView.vue') },
      {path:'forgot', component: () => import('../views/login/ForgotPasswordView.vue') },
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
