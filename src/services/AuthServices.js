export default class AuthService {
   getTreeNodes() {
        return fetch('/data/treenodes.json').then(res => res.json()).then(d => d.root);
    }
}